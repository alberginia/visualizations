# Visualization

In this repository you can find a few different figures and representations I have done for various projects.

 1. **Decision tree**

    * A gif image I created to explain [how decision tree classifier models work](https://gitlab.com/alberginia/visualizations/-/blob/main/decision_tree/decision_tree_whiteBG.gif) (with transparent and white backgrounds).
  
    <img src="decision_tree/decision_tree_whiteBG.gif" alt="Decision tree" width="60%"/>
  
 2. **Polymer brushes**
  
    * A slide explaining [the steps for the chemical synthesis of polymer brushes](https://gitlab.com/alberginia/visualizations/-/blob/main/polymer_brushes/13_Smart_Materials_Muriel_v1_p5.svg). First initating the silicon surface and then the polimerization.
    
    * A slide explaining [how the grafting density of the polymer brushes can be modified](https://gitlab.com/alberginia/visualizations/-/blob/main/polymer_brushes/13_Smart_Materials_Muriel_v1_p6.svg).
    
        
    * A slide depicting [how nanoparticle and polymer brush geometries influence the composite structure](https://gitlab.com/alberginia/visualizations/-/blob/main/polymer_brushes/2015-03-16_Xerrada_DPG2015_v3_p4.svg).

    <img src="polymer_brushes/13_Smart_Materials_Muriel_v1_p5.svg" alt="Decision tree" width="80%"/>
    <img src="polymer_brushes/13_Smart_Materials_Muriel_v1_p6.svg" alt="Decision tree" width="80%"/>
    <img src="polymer_brushes/2015-03-16_Xerrada_DPG2015_v3_p4.svg" alt="Decision tree" width="80%"/>
    
     
 3. **PRESC: Performance and Robustness Evaluation for Statistical Classifiers**
  
    * A [scheme of my ML Classifier Copies module](https://gitlab.com/alberginia/visualizations/-/blob/main/PRESC/ML-classifier-copying-package-diagram_v2.svg) in Mozilla's project PRESC. 
    
    <img src="PRESC/ML-classifier-copying-package-diagram_v2.svg" alt="PRESC's ML Classifier Copies module" width="100%"/>
    
    * The [logo](https://gitlab.com/alberginia/visualizations/-/blob/main/PRESC/classifier-bias-PRESC_v3_black_margin3.svg) I made for that project.
  
    <img src="PRESC/classifier-bias-PRESC_v3_black_margin3.svg" alt="PRESC logo" width="40%"/>
   
   
 4. **PhD Thesis: "Short Range Order in Disorderer Phases using Neutron Diffraction"**
  
    An [excerpt of my PhD Thesis](https://gitlab.com/alberginia/visualizations/-/blob/main/PhD_Thesis/PhDThesis_MRE_2014_selected_figures_extract.pdf) with some of the figures I developed:
    
    * A periodic table illustrating the interaction of different atoms with X-rays (cross sections).
    
    * A periodic table illustrating the interaction of different atoms with neutrons (cross sections).
    
    * A figure explaining the concept of neutron scattering cross sections.
    
    * Scheme of a neutron scattering experiment for a liquid or another disordered phase.
    
    * Quantum mechanical representation of a single neutron scattered by a nucleus (where the initial position and momentum of the neutron is not known).
    
    * Quantum mechanical representation of a single neutron scattered by a nucleus
    (where the neutron velocity before and after is perfectly known).
    
    * Quantum mechanical representation of a neutron beam scattered by a nucleus.
    
    * Scheme of the calculation of a differential momentum volume in spherical coordinates.
    
    * Scheme of the calculation of the volume occupied by each single momentum vector state.
    
    * Scheme of the calculation of the number of available momentum states.
  
    You can download the whole thesis from  [here](https://upcommons.upc.edu/handle/2117/95479).
  
    
## License
    
Except where otherwise noted, content on this work is licensed under a Creative Commons license: Attribution-NonCommercial-ShareAlike 3.0 Spain
  
